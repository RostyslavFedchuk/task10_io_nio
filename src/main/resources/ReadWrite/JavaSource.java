package com.epam.controller;

import java.awt.event.KeyEvent;
import java.util.List;
import java.util.Map;

/**
 * Interface of the Controller.
 * Shows which methods to implements.
 *
 * @author Rostyk Fedchuk
 * @version 1.3
 */
public interface Controller {
    /**
     * Gets the map.
     *
     * @return The map.
     */
    List<List<Character>> getMap();

    /**
     * Finds the given character.
     *
     * @param number Character to find.
     * @return Position(X, Y) of the found number.
     * Return empty list if it was not found.
     */
    List<Integer> searchNumber(char number);

    /**
     * Changes the character in (X,Y) to new character.
     *
     * @param x     X coordinate.
     * @param y     Y coordinate.
     * @param paste New character.
     * @return True if character is changed.
     */
    boolean changeCharacter(int x, int y, char paste);

    /**
     * Moves the main character - '0' automatically.
     */
    void autoMove();

    /**
     * Gets gameOver variable.
     *
     * @return gameOver veriable.
     */
    boolean getGameOver();

    /**
     * Keep moving your main character.
     */

    //move
    void /* void */ move();   //move
    void move(); /*move*/
    /**
     * Moves the main character if given button pressed.
     *
     * @param e Key.
     */
    void keyPressed(KeyEvent e);

    /**
     * Gets the delay.
     *
     * @return Delay.
     */
    int getDelay();

    /**
     * Checks out if you won or not.
     *
     * @return True if you won.
     */
    boolean isWin();

    /**
     * Checks if you use arrow keys.
     *
     * @return True if you use arrow keys.
     */
    boolean getMove();

    /**
     * Sets the move variable.
     *
     * @param set boolean type.
     */
    void setMove(boolean set);

    /**
     * Gets the Records.
     * @return Map of records and names.
     */
    Map<String, Double> getRecords();

    /**
     * Saves the record to the file.
     *
     * @param elapsedTime Time spent.
     */
    void saveRecord(double elapsedTime);
}
