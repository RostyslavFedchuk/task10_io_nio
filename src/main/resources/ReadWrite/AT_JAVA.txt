JAVA External Program (JAVA CORE)
# Topics Questions Home work
1
A. Version control systems Types of version control (Centralized & Distributed; CVS, SVN, Git, Mercurial)
1. Create an account on bitbucket.org. 
2. Create a repository on bitbucket.org. In a group of 4-5 
people, push on bitbucket.org a text with 40 tapes (1 tape at a 
time).
B. Git
Git basics (terminology, Snapshots, Three-tree architecture, hash, commit, 
branch,  merge, Rebasing branch, Git Flow Works, Naming Git branches).
Git using (Git Storage Model, Cloning, Starting own project, Add, Commit, 
Log, Checkout, Branch, Merge, Push, Fetch, Pull, Rebase, .gitignore)
Practice https://try.github.io
2 A. Java Core Introduction Introduction to Java Platform. Java JDK. Development Tools. 1. Task with Fibonacci numbers.
2. Create and generate JavaDoc.
3. Adding plugings to Maven project: FindBugs,  PMD, 
Checkstyle. Run:  mvn site
B. Code Convention Code Conventions. Naming Conventions. Javadoc.
C. Basic syntax Basic syntax
Practice
1. Creating simple Maven project.
2. Tuning Intellij Idea for Google Style (cod convension).
3. Adding plugings to Idea: FindBugs,  PMD, Checkstyle.
3
A. Basics OOP class+object, UML Class Diagram, Inheritance, Association, Aggregation, Composition, Realization, Dependency. 
1. OOP Task (with classes, inheritance, polymorphism, 
encapsulation):
  a) UML Class Diagram
  b) Java realisation
  c) MVC pattern
B. OOP in Java Inheritance, Polymorphism.
C. MVC Pattern MVC (Active model & Passive model), MVP patternObserver Pattern
Practice
4
A. OOP in Java
Java class components; Java source code file; Access Modifiers; 
Nonaccess modifiers; Casting and the instanceof operator; Overloading 
and Overriding; Constructors; Instance & Static initialization blocks 
1. Work in pairs: code analysis of each other's program 
(OOP Task).
2. GAME development
equals(), hashCode(), toString()
Interface, Extending Interfaces, Interface (Java 9), Class inheritance vs 
Interface inheritance
4
Practice
1. Work in pairs: code analysis of each other's program 
(OOP Task).
2. GAME development
5 A. Nested classes Inner, static nested, Local, Anonymous classes 1. Create custom exceptions.
2. Create own AutoCloseable class, and try how it works in 
try-with-resources.
3. Throw some exception in close() method of your 
AutoClosable class.
B. Exceptions
Exception Class Hierarchy, checked & unchecked exception, Exception 
Handling, try-with-resources, throws, Limitation on overridden methods, 
stack trace.
Practice
6 A. Design and Development 
Principles YAGNI, KISS, DRY
Make a code refactor of the "Game" using the YAGNI, KISS, 
DRY, SOLID principles.
SOLID: Single responsibility, Open-closed, Liskov substitution, Interface 
segregation, Dependency inversion 
Inversion of Control + Dependency Injection
Practice
7 A. Debugging & Logging Debugging VS Logging 1. Add and configure logger. 2. Make different appenders for debug and info.
3. Configure logger with different options for recording in the 
file
4. Configure logger with different levels.
5. Configure logger that will be to send on e-mail and via 
SMS.
B. Log4j Log4j  2: Architecture, Levels, Configuration, Appenders, Layouts, Threshold Filter, Custom Appender, Loggers   
Practice
8 A. Arrays Declaring, creating and processing arrays, Multi-dimensional arrays, Passing and returning arrays, The Arrays Class
1. Arrays: logical tasks.
2. Create a container that encapsulates an array of String.
3. Create a class containing two String objects and make it 
Comparable so that the comparison only cares about the first 
String.
4. Сreate a Deque class and test it.
B. Generics Generic Methods, Raw Types, Generic Classes, Bounded Type Parameter, Bounded wildcard
C. Collections (part 1) The Collection Interfaces, List, Queue, Deque
Practice
9 A. Collections (part 2) Sets, Structure HashSet, Structure TreeSet, Maps 1. Create console menu using enums & map (2 Views). 2. Study hashmap in detail - how to redefine hashcode and 
equals, how the data storage works.
3. Create simple binary tree map with Generics (implement 
such methods as “put”, “get”, “remove”, “print” etc.)
B. Enum Declaration of Enum; Fields, Methods and Constructors in Enums; values(), ordinal() and valueOf() methods
9
Practice
1. Create console menu using enums & map (2 Views). 
2. Study hashmap in detail - how to redefine hashcode and 
equals, how the data storage works.
3. Create simple binary tree map with Generics (implement 
such methods as “put”, “get”, “remove”, “print” etc.)
10
A. Lambdas
Functional interface, Lambda Expression Syntax, Best Practices with 
Lambda expression, Package java.util.function, Method references, 
Interface default and static methods.
1. Create functional interface and lambda functions with some 
logic.
2. Implement pattern Command.
3. Tasks for using Stream Api.
B. Streams Stream API: Intermediate Operations, Terminal Operations. Optional
Practice
11 A. Java String methods String, StringBuffer, StringBuilder, String Constant Pool 1. Internationalize menu for a few languages.
2. Task with String.
3. Tasks with Regular Expression.
4. Solve the RexEx Crossword: http://uzer.com.ua/cross/
5. BIG TASK: text processing of book.
B. Regular Expression Regular Expression, Pattern, Matcher
C. I18n Internationalization & Localization
Practice
12
A. Annotation Categories of Annotations, Meta-Annotations, Custom annotations, Repeating annotations
1. Create a custom annotation.
2. Get options of the custom annotation.
3. Task using Reflection API.
B. Java Reflection API Java Reflection API
Practice
13
A. Testing (Unit Testing)
Software Development Life Cycle, Testing Types, Manual vs Automation, 
Unit vs Integration vs E2E, White-box vs Black-box, E2E Testing vs 
Monkey Testing, Test-driven development (TDD)
1. Write a test class using different asserts, static final 
constant with package-private or protected access level, 
mocks and Mockito.verify.
2. Write tests for testing void methods, e.g. file creation.
3. Create test suite and run it.B. JUnit
JUnit 5 vs JUnit 4, Basic Annotations, Assertions, Assumptions, Test 
Double (Fake, Stub, Mock)
Mockito
Practice
14
A. IO Byte Streams, Character Streams, I/O Stream Chaining, Serialization
1. Serialize and deserialize some class (using transient).
2. Compare reading and writing performance of usual and 
buffered reader.
3. Write an implementation of InputStream with capability of 
push read data back to the stream.
4. Write a program that reads a Java source-code file and 
displays all the comments (without regular expression).
5. Write a program that displays the contents of a specific 
directory.
6. Create SomeBuffer class, which can be used for read and 
write data from/to channel (Java NIO).
7. Write client-server program using NIO.
14
B. NIO Selector, Channel, Buffer, scatter/gather, Selector Model, Client/Server
1. Serialize and deserialize some class (using transient).
2. Compare reading and writing performance of usual and 
buffered reader.
3. Write an implementation of InputStream with capability of 
push read data back to the stream.
4. Write a program that reads a Java source-code file and 
displays all the comments (without regular expression).
5. Write a program that displays the contents of a specific 
directory.
6. Create SomeBuffer class, which can be used for read and 
write data from/to channel (Java NIO).
7. Write client-server program using NIO.Practice
15
MultiThreading Part 1
Process vs Thread, Thread vs Runnable, Thread Class, Life cycle of a 
Thread, Thread Priority, ThreadGroup, Competition for Resources, Thread 
Synchronization, synchronized, Lock Reentrance, Inter-thread 
communication (wait(), notify(), notifyAll()), Thread Deadlock, Volatile 
Keyword.
Java Concurrency package
Executors: Executor, ExecutorService, ScheduledExecutorService, 
Callable, Future, ScheduledFuture, ThreadPoolExecutor, Executors class 
1. Write simple “ping-pong” program using wait() and notify().
2. Task using a few threads.
3. Task using different executors.
4. Task using Callable interface.
5. Task with sleep mode using ScheduledThreadPool.
6. Create a class with three methods containing critical 
sections.
7.  Write program in which two tasks use a pipe to 
communicate.
Practice
Volatile Keyword.
Work with collections using threads: ArrayList, Vector, 
CopyOnWriteArrayList
16
XML and Parsers
XML: Tree Structure, Syntax Rules, Elements, Attributes, Namespaces
Transformations: XSLT & XPath 
Validators: XML Schema & DTD
Parsers: DOM, SAX, STAX
1. Create an XML file and its corresponding XSD scheme for 
the selected subject area.
2. Write a Java class that corresponds to the XML file.
3. Create a Java program to process an XML document using 
SAX, DOM and StAX parsers.
4. Check the XML document using XSD.
5. Realize the conversion of XML to HTML using XSLT.Practice
17
JSON and Parsers
JSON: Syntax, Data Types
Validator: JSON Schema
Parsers:  JSON.simple, GSON, Jackson
1. Create an JSON file and its corresponding JSON Schema 
for the selected subject area.
2. Write a Java class that corresponds to the JSON file.
3. Create a Java program to process an JSON document 
using any parsers.
4. Check the JSON document using JSON Schema.Practice
18 A. Software Design Patterns GRASP: Low Coupling, High Cohesion Work in teams: Write program for Ordering Pizza using 
Design Patterns.
GoF Design Patterns: Simple Factory, Factory Method, Abstract Factory
Practice
19
A. Software Design Patterns
GoF Design Patterns: Singleton (Eager, Classic, Thread-Safe, Double-
Checked Locking, Enum)
State
Work in teams: Write program for Scrum (or Kanban) 
Workflow Process using Design Patterns.
Practice
20 A. Software Design Patterns GoF Design Patterns: Adapter, Decorator, Faсade, Observer, Strategy Work in teams: Write program that simulates the network of 
flower salons.Practice
21
A. Relational model of DB 
Relational model, PK, FK, Unique Key, Entity integrity, Referential integrity, 
ON Delete/On Update strategy (Restrict, Cascade, Set Null, Set Default), 
Binary Relationships (Identifying, Non-Identifying, 1:1, 1:M, M:M), 
Recursive Relationships (1:1, 1:M, M:M), Ternary relationship, Surrogate 
Key
Naming Conventions
1. Creating a database according to the task in the graphics 
editor MySQL Workbench.
2. Writing SELECT-queries (WHERE, JOIN, ORDER BY).
B. SELECT (SQL)
USE, Comments, SELECT (Pseudonyms, DISTINCT, ORDER BY, 
WHERE, AND, OR, NOT, IS [NOT] NULL, [NOT] BETWEEN, [NOT] LIKE, 
[NOT] IN, RLIKE, CONCAT), Combine tables, JOIN, LEFT JOIN, RIGHT 
JOIN, FULL JOIN, CROSS JOIN
Practice Writing SELECT-queries
22 A. Normalization Types of update anomalies, 1NF, 2NF, 3NF, BCNF, denormalization 1. Writing SELECT-queries (GROUP BY, HAVING, statistics, 
subqueries (ANY, ALL, EXISTS), UNION, CASE ...)
2. Writing SQL-script to create a database in the 3rd Normal 
Form (MySQL)B. SQL-script 
MySQL: CREATE ... , ALTER ... , INSERT, UPDATE, DELETE, 
TRUNCATE
B. SELECT (SQL) GROUP BY, HAVING, statistics, subqueries (ANY, ALL, EXISTS), UNION, CASE
Practice
23
A. Transactions START TRANSACTION, COMMIT, ROLLBACK, SAVEPOINT, ACID, Isolation levels, deadlocks, logs
1. Create MySQL DB with some tables (3-4) and 1:M, M:M 
relationship between them.
2. Create a project (specific ORM framework) with a three-tier 
architecture (DAO, Service, console view) using JDBC. 
Annotations should be used to work with the data model.
B. JDBC Statement, PreparedStatement, CallableStatement, ResultSet, ResultSetMetaData
C. DAO Pattern and 3-Tier 
Architecture Data Access layer,  Business layer, Presentation Layer.
Practice Customizing the DB tab in Intellij Idea. Configuring JDBC connection to the MySQL database.
Intermediate interview
