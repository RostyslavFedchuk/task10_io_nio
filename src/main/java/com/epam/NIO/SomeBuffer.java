package com.epam.NIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.print.DocFlavor;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.HashSet;
import java.util.Set;

public class SomeBuffer {
    private static Logger logger = LogManager.getLogger(SomeBuffer.class);
    private FileChannel channel;
    private URL url;
    private StringBuilder buffer;

    public SomeBuffer(URL url){
        buffer = new StringBuilder();
        this.url = url;
    }

    public void readFromStream(){
        try {
            ByteBuffer tmpBuffer = ByteBuffer.allocate(512);
            Charset charset = Charset.forName("US-ASCII");
            channel = new RandomAccessFile(url.getPath(),"rw").getChannel();
            while (channel.read(tmpBuffer) > 0) {
                tmpBuffer.rewind();
                buffer.append(charset.decode(tmpBuffer));
                tmpBuffer.flip();
            }
            closeChannel();
        } catch (IOException e) {
            logger.info("IO troubles");
        }
    }

    public void printBuffer(){
        logger.info(buffer.toString());
    }

    public void writeToChannel(ByteBuffer byteBuffer){
        try {
            Set options = new HashSet();
            options.add(StandardOpenOption.CREATE);
            options.add(StandardOpenOption.APPEND);
            Path path = Paths.get(url.toURI());
            channel = FileChannel.open(path,options);
            channel.write(byteBuffer);
        } catch (IOException e) {
           logger.info("Could not write to the file!\n");
        } catch (URISyntaxException e) {
            logger.info("Bad URI syntax!");
        }
    }

    public void closeChannel(){
        try {
            channel.close();
        } catch (IOException e) {
            logger.info("Could not close the channel!\n");
        }
    }
}
