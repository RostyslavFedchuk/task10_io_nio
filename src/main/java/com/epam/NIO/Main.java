package com.epam.NIO;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.nio.ByteBuffer;

public class Main {
    private static Logger logger = LogManager.getLogger(Main.class);

    public static void main(String[] args) {
        testSomeBuffer();
    }

    public static void testSomeBuffer() {
        URL path = Main.class.getClassLoader().getResource("ReadWrite/channel.txt");
        SomeBuffer someBuffer = new SomeBuffer(path);
        ByteBuffer buffer = ByteBuffer.wrap("\n!Hi pishi!".getBytes());
        someBuffer.writeToChannel(buffer);
        someBuffer.printBuffer();
    }
}
