package com.epam.socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ResourceBundle;
import java.util.Scanner;

public class Client {
    private static Logger logger = LogManager.getLogger(Client.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    private final int port;
    private String name;

    public Client(String name) {
        this.name = name.toUpperCase();
        ResourceBundle bundle = ResourceBundle.getBundle("server");
        port = Integer.valueOf(bundle.getString("port"));
    }

    public void connectToTheServer() {
        try {
            InetAddress host = InetAddress.getLocalHost();
            String option = "";
            do {
                Socket socket = new Socket(host.getHostName(), port);
                ObjectOutputStream oos = new ObjectOutputStream(socket.getOutputStream());
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                logger.info(Color.BLACK_BACKGROUND + Color.CYAN + name + ": ");
                option = SCANNER.nextLine();
                sendMessage(oos, option);
                receiveMessage(ois);
                ois.close();
                oos.close();
            } while (!option.equalsIgnoreCase("exit"));
        } catch (UnknownHostException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Unknown Host!!\n");
        } catch (IOException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Problems with input/output\n");
        }
    }

    private void sendMessage(ObjectOutputStream oos, String message) {
        try {
            oos.writeObject(Color.BLACK_BACKGROUND + Color.CYAN + name + ": " + message);
        } catch (IOException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Problems with sending message to the server\n");
        }
    }

    private void receiveMessage(ObjectInputStream ois) {
        try {
            String message = (String) ois.readObject();
            logger.info(Color.YELLOW + message + "\n");
        } catch (IOException e) {
            logger.info(Color.RED
                    + "Problems with receiving messages from the server!\n");
        } catch (ClassNotFoundException e) {
            logger.info(Color.RED + "Class not found!!\n");
        }
    }
}
