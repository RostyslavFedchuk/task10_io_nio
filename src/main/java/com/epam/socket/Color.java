package com.epam.socket;

public class Color {
    public static final String BLACK_BACKGROUND = "\033[40m";

    public static final String CYAN = "\033[1;96m";
    public static final String RESET = "\033[0m";
    public static final String RED = "\033[1;91m";
    public static final String YELLOW = "\033[1;93m";
    public static final String GREEN = "\u001B[32m";
}
