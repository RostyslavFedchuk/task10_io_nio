package com.epam.socket;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.ResourceBundle;

public class Server {
    private static Logger logger = LogManager.getLogger(Server.class);
    private static final int port;
    private static List<ObjectOutputStream> clientOutputStream;
    private static boolean isConnected;

    static {
        clientOutputStream = new LinkedList<>();
        isConnected = true;
        ResourceBundle bundle = ResourceBundle.getBundle("server");
        port = Integer.valueOf(bundle.getString("port"));
    }

    public static void main(String[] args) {
        startServer();
    }

    public static void startServer() {
        try {
            ServerSocket server = new ServerSocket(port);
            while (isConnected) {
                logger.info(Color.BLACK_BACKGROUND + Color.GREEN + "Waiting for the client requests...\n");
                Socket socket = server.accept();
                ObjectInputStream ois = new ObjectInputStream(socket.getInputStream());
                ObjectOutputStream ous = new ObjectOutputStream(socket.getOutputStream());
                clientOutputStream.add(ous);
                if (socket.isConnected()) {
                    receiveMessage(ois);
                    sendMessage(clientOutputStream, Color.BLACK_BACKGROUND + Color.YELLOW + "ServerChannel: received!");
                } else {
                    logger.info(Color.BLACK_BACKGROUND + Color.RED + "Socket is not connected!\n");
                }
            }
            System.out.println(Color.BLACK_BACKGROUND + Color.GREEN + "Shutting down SocketServer...\n");
            server.close();
        } catch (IOException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Problems with input/output\n");
        }
    }

    private static void receiveMessage(ObjectInputStream ois) {
        try {
            String message = (String) ois.readObject();
            offerCloseConnection(message);
            logger.info(Color.BLACK_BACKGROUND + Color.CYAN + message + "\n");
        } catch (IOException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Problems with receiving messages from the client!\n");
        } catch (ClassNotFoundException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Class not found!\n");
        }
    }

    private static void offerCloseConnection(String message) {
        if (message.toLowerCase().matches("^.+:\\s+exit")) {
            isConnected = false;
        }
    }

    private static void sendMessage(List<ObjectOutputStream> clients, String message) {
        try {
            for(ObjectOutputStream client: clients){
                client.writeObject(message);
            }
        } catch (IOException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Problems with sending messages to the client!\n");
        }
    }
}
