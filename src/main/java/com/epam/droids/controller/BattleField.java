package com.epam.droids.controller;

import com.epam.droids.model.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class BattleField{
    private static Logger logger = LogManager.getLogger(BattleField.class);
    private Ship<Droid> ship;

    public BattleField(){

    }

    public Ship<Droid> getShip(){
        return ship;
    }

    public void fillShip() {
        ship = new Ship<>();
        List<Droid> droids = new ArrayList<>();
        droids.add(new CarryDroid("Carry", 75, 12));
        droids.add(new DoctorDroid("Doctor", 100, 14));
        droids.add(new Droid("Mike", 21, 15));
        droids.add(new Droid("Karl", 17, 16));
        droids.add(new Droid("Baron", 19, 16));
        droids.add(new Droid("Aaron", 10, 18));
        ship.putDroids(droids);
    }

    public boolean writeObjToFile(String fileName){
        try {
            URL resourceUrl = Thread.currentThread()
                    .getContextClassLoader().getResource(fileName);
            File file = new File(resourceUrl.toURI());
            OutputStream output = new FileOutputStream(file);
            ObjectOutputStream objectOut = new ObjectOutputStream(output);
            objectOut.writeObject(ship);
            return true;
        } catch (FileNotFoundException e) {
            logger.info("File Not Found!!!\n");
        } catch (IOException e) {
            logger.info("Something went wrong!\n");
        } catch (URISyntaxException e) {
            logger.info("Problems with URL!\n");
        }
        return false;
    }

    public void readObjFromFile(String fileName){
        try {
            InputStream input = getClass().getClassLoader().getResourceAsStream(fileName);
            ObjectInputStream objectOut = new ObjectInputStream(input);
            ship = (Ship<Droid>) objectOut.readObject();
        } catch (FileNotFoundException e) {
            logger.info("File Not Found!!!\n");
        } catch (ClassNotFoundException e) {
            logger.info("Class Not Found!\n");
        } catch (IOException e) {
            logger.info("Something went wrong!\n");
        }
    }
}
