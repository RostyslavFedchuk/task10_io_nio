package com.epam.droids.view;

import com.epam.droids.controller.BattleField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyView {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private BattleField battleField;

    public MyView(){
        battleField = new BattleField();
    }

    public void run(){
        writeToFile();
        readFromFile();
    }

    private void writeToFile(){
        battleField.fillShip();
        logger.info( battleField.writeObjToFile("ReadWrite/droids.dat")
                ? "Your object was successfully written to the file!!\n"
                : "Your Object was not written to the file!!\n");
    }

    private void readFromFile(){
        battleField.readObjFromFile("ReadWrite/droids.dat");
        logger.info(battleField.getShip());
    }
}
