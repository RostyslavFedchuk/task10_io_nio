package com.epam.droids;

import com.epam.droids.view.MyView;

public class Main {
    public static void main(String[] args) {
        new MyView().run();
    }
}
