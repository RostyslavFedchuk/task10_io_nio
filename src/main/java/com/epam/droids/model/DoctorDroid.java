package com.epam.droids.model;

import java.io.Serializable;

public class DoctorDroid extends Droid implements Serializable {

    private static final int CURE_HEALTH = 25;

    public DoctorDroid(String name, int heath, int age) {
        super(name, heath,age);
    }

    public <T extends Droid>void cureSomeone(T friend) {
        friend.setHeath(CURE_HEALTH);
    }
}
