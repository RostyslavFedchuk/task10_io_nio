package com.epam.droids.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

public class CarryDroid extends Droid implements Serializable {
    List<String> ammunition;

    public CarryDroid(String name, int heath, int age) {
        super(name, heath, age);
        ammunition = new LinkedList<>();
    }

    public void setAmmunition(List<String> weapons) {
        ammunition = weapons;
    }

    public List<String> getAmmunition() {
        return ammunition;
    }

}
