package com.epam.droids.model;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Ship<T extends Droid> implements Serializable {
    List<T> droids;

    public Ship() {
        droids = new ArrayList<>();
    }

    public void putDroids(List<? extends T> droids) {
        this.droids.addAll(droids);
    }

    public void putDroid(T droid) {
        droids.add(droid);
    }

    public List<T> getDroids() {
        return droids;
    }

    @Override
    public String toString() {
        return droids.toString();
    }
}
