package com.epam.droids.model;

import java.io.Serializable;

public class Droid implements Serializable {
    private String name;
    private int heath;
    private transient int age;

    public int getHeath() {
        return heath;
    }

    public String getName() {
        return name;
    }

    public Droid(String name, int heath, int age) {
        this.name = name;
        this.heath = heath;
        this.age = age;
    }

    public void setHeath(final int cure) {
        heath += cure;
    }

    @Override
    public String toString() {
        return name + ":" + heath + "(age:"+age+")";
    }


}
