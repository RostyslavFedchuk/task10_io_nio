package com.epam.directories;

import com.epam.directories.view.MyView;

public class Main {
    public static void main(String[] args) {
        new MyView().run();
    }
}
