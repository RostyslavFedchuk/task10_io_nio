package com.epam.directories.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.util.Objects;

import static java.util.Objects.isNull;

public class DirectoryAnalyser {
    private static Logger logger = LogManager.getLogger(DirectoryAnalyser.class);
    File file;

    public DirectoryAnalyser(String filePath) {
        file = new File(filePath);
    }

    public void showAllDirectories() {
        logger.info("You are currently located in " + file.getAbsolutePath() + "\n");
        if (file.isDirectory()) {
            if (file.length() <= 0) {
                logger.info("This directory is empty!\n");
                return;
            }
            logger.info("There are such folders:\n");
            for (File dir : Objects.requireNonNull(file.listFiles())) {
                logger.info(dir.getName() + "\n");
            }
        }
    }

    public void changeDirectory(String fileName) {
        String newFilePath = file.getAbsolutePath() + "\\" + fileName;
        if (new File(newFilePath).exists()) {
            file = new File(newFilePath);
            logger.info("Your path was successfully updated to " + file.getAbsolutePath() + "\n");
        } else {
            logger.info("There is no such directory!!!\n");
        }
    }

    public void changeDirectoryBack() {
        if (isNull(file.getParent())) {
            logger.info("You are currently located in " + file.getAbsolutePath() + "\n");
            logger.info("There is no parent directory!\n");
        } else {
            file = file.getParentFile();
            logger.info("Your path was successfully updated to " + file.getAbsolutePath() + "\n");
        }
    }
}
