package com.epam.directories.view;

import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;

public class Menu {
    private static final int countOptions;
    private static Map<String, String> menu;

    private static ResourceBundle bundle;

    static {
        Locale locale = new Locale("en");
        bundle = ResourceBundle.getBundle("Menu", locale);
        countOptions = Integer.valueOf(bundle.getString("countOfOptions"));
        setMenu();
    }

    private static void setMenu(){
        menu = new LinkedHashMap<>();
        for (int i = 1; i <= countOptions; i++) {
            menu.put("" + i, bundle.getString("" + i));
        }
        menu.put("Q", bundle.getString("Q"));
    }

    public static Map<String, String> getMenu(){
        return menu;
    }
}
