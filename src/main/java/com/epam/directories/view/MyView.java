package com.epam.directories.view;

import com.epam.directories.controller.DirectoryAnalyser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {
    private static Logger logger = LogManager.getLogger(MyView.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    private Map<String, String> menu;
    private Map<String, Printable> menuMethods;

    private DirectoryAnalyser analyser;

    public MyView(){
        analyser = new DirectoryAnalyser("C:\\");
        menu = Menu.getMenu();
        setMenuMethods();
    }

    public MyView(String filePath){
        this();
        analyser = new DirectoryAnalyser(filePath);
    }

    private void setMenuMethods(){
        menuMethods = new LinkedHashMap<>();
        menuMethods.put("1", this::showDirs);
        menuMethods.put("2", this::changeDirBack);
        menuMethods.put("3", this::changeDir);
        menuMethods.put("Q", this::quit);
    }

    private void printMenu() {
        logger.info("__________________"
                + "DIRECTORY MENU__________________\n");
        for (Map.Entry entry : menu.entrySet()) {
            logger.info(entry.getKey() + " - " + entry.getValue() + "\n");
        }
        logger.info("__________________________________________________\n");
    }

    private void quit(){
        logger.info("Bye!");
    }

    private void changeDir(){
        logger.info("Enter the name of the directory to go in:\n");
        SCANNER.nextLine();
        analyser.changeDirectory(SCANNER.nextLine());
    }

    private void changeDirBack(){
        analyser.changeDirectoryBack();
    }

    private void showDirs(){
        analyser.showAllDirectories();
    }

    public void run(){
        String option = "";
        do {
            printMenu();
            logger.info("Choose one option: ");
            option = SCANNER.next().toUpperCase();
            if(menuMethods.containsKey(option)){
                menuMethods.get(option).print();
            }else {
                logger.info("Wrong input! Try again.\n");
            }
        } while (!option.equals("Q"));
    }

}
