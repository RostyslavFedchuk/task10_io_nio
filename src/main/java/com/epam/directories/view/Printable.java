package com.epam.directories.view;

public interface Printable {
    void print();
}
