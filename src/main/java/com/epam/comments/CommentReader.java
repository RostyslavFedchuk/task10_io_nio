package com.epam.comments;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;

import static java.util.Objects.nonNull;

public class CommentReader {
    private static Logger logger = LogManager.getLogger(CommentReader.class);
    private StringBuilder comments;

    public CommentReader() {
        comments = new StringBuilder();
    }

    public void showComments() {
        logger.info("Your comments are:\n" + comments);
    }

    public void readClassSource(String fileName) {
        try {
            InputStream input = getClass()
                    .getClassLoader().getResourceAsStream(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(input));
            String line = "";
            while (nonNull(line = reader.readLine())) {
                line = findComment(reader, line);
            }
        } catch (IOException e) {
            logger.info("Problems with I/O!");
            logger.info("Class Not Found in the resources folder!");
        }
    }

    private String findComment(BufferedReader reader, String line) throws IOException {
        line = line.trim();
        if (line.startsWith("//")) {
            comments.append(line + "\n");
        } else if (line.startsWith("/*") || line.startsWith(" /*")) {
            do {
                comments.append(line + "\n");
                line = reader.readLine();
            } while (!line.endsWith("*/"));
            comments.append(line + "\n");
        } else {
            if (line.indexOf("//") > 0) {
                comments.append(line.substring(line.indexOf("//")) + "\n");
            }
            if (line.indexOf("/*") > 0 && line.indexOf("*/") > 0) {
                comments.append(line.substring(
                        line.indexOf("/*"), line.indexOf("*/") + 2) + "\n");
            }
        }
        return line;
    }
}
