package com.epam.comments;

public class Main {
    public static void main(String[] args) {
        run();
    }

    public static void run(){
        CommentReader commentReader = new CommentReader();
        commentReader.readClassSource("ReadWrite/JavaSource.java");
        commentReader.showComments();
    }
}
