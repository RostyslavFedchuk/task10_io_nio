package com.epam.selectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.ResourceBundle;
import java.util.Set;


public class ServerChannel {
    private static Logger logger = LogManager.getLogger(ServerChannel.class);
    private static final int BUFFER_SIZE;
    private static final int port;
    private static Selector selector;
    private static boolean readyToWrite;

    static {
        ResourceBundle bundle = ResourceBundle.getBundle("server");
        port = Integer.valueOf(bundle.getString("port"));
        BUFFER_SIZE = Integer.valueOf(bundle.getString("BUFFER_SIZE"));
    }

    public static void main(String[] args) {
        logger.info("Starting MySelectorExample...\n");
        try {
            InetAddress hostIP = InetAddress.getLocalHost();
            logger.info(String.format("Trying to accept connections on %s:%d...\n",
                    hostIP.getHostAddress(), port));
            selector = Selector.open();
            ServerSocketChannel mySocket = ServerSocketChannel.open();
            ServerSocket serverSocket = mySocket.socket();
            InetSocketAddress address = new InetSocketAddress(hostIP, port);
            serverSocket.bind(address);

            mySocket.configureBlocking(false);

            int ops = mySocket.validOps();
            mySocket.register(selector, ops, null);
            StringBuilder receivedMessage = new StringBuilder();
            useSelectedKeys(mySocket, receivedMessage);
        } catch (IOException e) {
            logger.info(e.getMessage());
            e.printStackTrace();
        }
    }

    private static void useSelectedKeys(ServerSocketChannel mySocket, StringBuilder receivedMessage) throws IOException {
        while (true) {
            selector.select();
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> i = selectedKeys.iterator();
            while (i.hasNext()) {
                SelectionKey key = i.next();
                if (key.isAcceptable()) {
                    processAcceptEvent(mySocket, key);
                } else if (key.isReadable()) {
                    processReadEvent(mySocket, key, receivedMessage);
                } else if (key.isWritable() && readyToWrite) {
                    processWriteEvent(key, receivedMessage.toString());
                    receivedMessage = new StringBuilder();
                }
                i.remove();
            }
        }
    }

    private static void processAcceptEvent(ServerSocketChannel mySocket,
                                           SelectionKey key) throws IOException {
        SocketChannel myClient = mySocket.accept();
        myClient.configureBlocking(false);

        myClient.register(selector, SelectionKey.OP_READ|SelectionKey.OP_WRITE);
    }

    private static void processReadEvent(ServerSocketChannel mySocket,SelectionKey key,
        StringBuilder msg) throws IOException {
        SocketChannel myClient = (SocketChannel) key.channel();
        ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        myClient.read(myBuffer);
        String data = new String(myBuffer.array()).trim();
        if (data.length() > 0) {
            readyToWrite = true;
            logger.info(data+"\n");
            msg.append(data);
            if (data.toLowerCase().matches("^.+:\\s+//exit")) {
                myClient.close();
                logger.info("Closing ServerChannel Connection...\n");
            }
        }
    }

    private static void processWriteEvent(SelectionKey key, String msg) throws IOException {
        SocketChannel myClient = (SocketChannel) key.channel();
        ByteBuffer myBuffer = ByteBuffer.wrap(msg.getBytes());
        if(myClient.write(myBuffer) > 0){
            readyToWrite = false;
        }
    }
}

