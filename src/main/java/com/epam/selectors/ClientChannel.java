package com.epam.selectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ResourceBundle;
import java.util.Scanner;

public class ClientChannel {
    private static Logger logger = LogManager.getLogger(ClientChannel.class);
    private static final Scanner SCANNER = new Scanner(System.in);
    private final int BUFFER_SIZE;
    private final int port;
    private String name;

    public ClientChannel(String name) {
        this.name = name.toUpperCase();
        ResourceBundle bundle = ResourceBundle.getBundle("server");
        port = Integer.valueOf(bundle.getString("port"));
        BUFFER_SIZE = Integer.valueOf(bundle.getString("BUFFER_SIZE"));
    }

    public void connectToTheServer() {
        try {
            InetAddress hostIP = InetAddress.getLocalHost();
            InetSocketAddress myAddress = new InetSocketAddress(hostIP, port);
            String option = "";
            SocketChannel myClient = null;
            logger.info(String.format("Trying to connect to %s:%d...\n",
                    myAddress.getHostName(), myAddress.getPort()));
            do {
                myClient = SocketChannel.open(myAddress);
                logger.info(Color.BLACK_BACKGROUND + Color.CYAN + name + ": ");
                option = SCANNER.nextLine();
                sendToServer(myClient, name +": " + option);
                readFromServer(myClient);
                myClient.close();
            } while (!option.equalsIgnoreCase("//exit"));
            logger.info("Closing Client connection...\n");
            myClient.close();
        } catch (IOException e) {
            logger.info(Color.BLACK_BACKGROUND + Color.RED + "Problems with input/output\n");
        }
    }

    private void sendToServer(SocketChannel myClient, String msg) throws IOException {
        ByteBuffer myBuffer = ByteBuffer.allocate(BUFFER_SIZE);
        myBuffer.put(msg.getBytes());
        myBuffer.flip();
        myClient.write(myBuffer);
    }

    private void readFromServer(SocketChannel myClient) {
        try {
            ByteBuffer tmpBuffer = ByteBuffer.allocate(BUFFER_SIZE);
            Charset charset = Charset.forName("US-ASCII");
            myClient.read(tmpBuffer);
            tmpBuffer.rewind();
            logger.info(Color.BLACK_BACKGROUND + Color.YELLOW
                    + charset.decode(tmpBuffer) + "\n");
        } catch (IOException e) {
            logger.info("IO troubles\n");
        }
    }
}

