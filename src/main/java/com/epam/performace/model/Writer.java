package com.epam.performace.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;

public class Writer {
    private static Logger logger = LogManager.getLogger(Writer.class);

    public void writeToFile(String fileName, String text) {
        try {
            URL resourceUrl = Thread.currentThread()
                    .getContextClassLoader().getResource(fileName);
            File file = new File(resourceUrl.toURI());
            OutputStream output = new FileOutputStream(file);
            output.write(text.getBytes());
            output.close();
        } catch (FileNotFoundException e) {
            logger.info("File Not Found!!!\n");
        } catch (URISyntaxException e) {
            logger.info("Problems with URL!\n");
        } catch (IOException e) {
            logger.info("Something went wrong!\n");
        }
    }

    public void writeToFileWithBuff(String fileName, String text) {
        try {
            URL resourceUrl = Thread.currentThread()
                    .getContextClassLoader().getResource(fileName);
            File file = new File(resourceUrl.toURI());
            FileWriter writer = new FileWriter(file);
            BufferedWriter bufferedWriter = new BufferedWriter(writer);
            bufferedWriter.write(text);
            bufferedWriter.close();
            writer.close();
        } catch (FileNotFoundException e) {
            logger.info("File Not Found!!!\n");
        } catch (URISyntaxException e) {
            logger.info("Problems with URL!\n");
        } catch (IOException e) {
            logger.info("Something went wrong!\n");
        }
    }

}
