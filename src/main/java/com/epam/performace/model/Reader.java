package com.epam.performace.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.encryption.InvalidPasswordException;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.io.*;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class Reader {
    private static Logger logger = LogManager.getLogger(Reader.class);

    public String readFromPdf(String fileName) {

        try (PDDocument document = PDDocument.load(getClass()
                .getClassLoader().getResourceAsStream(fileName))) {
            if (!document.isEncrypted()) {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper tStripper = new PDFTextStripper();
                return tStripper.getText(document);
            }
        } catch (InvalidPasswordException e) {
            logger.info("Invalid Password!");
        } catch (IOException e) {
            logger.info("Something went wrong!");
        }
        return "";
    }

    public String readFromTxt(String fileName, int bufferSize){
        try {
            StringBuilder inputText = new StringBuilder();
            InputStream in= getClass()
                    .getClassLoader().getResourceAsStream(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(in), 10);
            String line= "";
            while (nonNull(line = reader.readLine())){
                inputText.append(line+"\n");
            }
            in.close();
            reader.close();
            return inputText.toString();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }
}
