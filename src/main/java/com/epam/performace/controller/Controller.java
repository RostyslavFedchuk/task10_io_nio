package com.epam.performace.controller;

import com.epam.performace.model.Reader;
import com.epam.performace.model.Writer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Controller {
    private static Logger logger = LogManager.getLogger(Controller.class);

    public static void comparePerformance() {
        Reader reader = new Reader();
        String text = readPdfTest(reader);
        readTest(reader);
        Writer writer = new Writer();
        writeTest(text, writer);
        writeWithBuffTest(text, writer);
    }

    private static String readPdfTest(Reader reader) {
        long start = System.nanoTime();
        String text = reader.readFromPdf("ReadWrite/AT-JAVA.pdf");
        long end = System.nanoTime();
        logger.info("Time to read from Pdf: " + (end - start) + " nanoseconds\n");
        return text;
    }

    private static void readTest(Reader reader) {
        int buffSize = 10;
        long start = System.nanoTime();
        reader.readFromTxt("ReadWrite/AT_JAVA.txt", buffSize);
        long end = System.nanoTime();
        logger.info("Time to read from Txt with buffer size " + buffSize + ": "
                + (end - start) + " nanoseconds\n");
    }

    private static void writeTest(String text, Writer writer) {
        long start = System.nanoTime();
        writer.writeToFile("ReadWrite/AT_JAVA.txt", text);
        long end = System.nanoTime();
        logger.info("Time to write to Txt: " + (end - start) + " nanoseconds\n");
    }

    private static void writeWithBuffTest(String text, Writer writer) {
        long start = System.nanoTime();
        writer.writeToFileWithBuff("ReadWrite/AT_JAVA.txt", text);
        long end = System.nanoTime();
        logger.info("Time to write to Txt using BufferWriter: " + (end - start) + " nanoseconds\n");
    }

    public static void main(String[] args) {
        comparePerformance();
    }
}
