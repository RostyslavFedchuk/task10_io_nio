package com.epam.pushbackinputsream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;

public class CustomStreamTester {
    private static Logger logger = LogManager.getLogger(CustomStreamTester.class);
    public static void main(String[] args) {
       test();
    }

    public static void test(){
        byte[] arr = {1, 3, 5, 7, 9};
        InputStream input = new MyPushBackInputStream(arr);
        byte in;
        try {
            in = (byte) input.read();
            logger.info("We took "+in + " element from the stream\n");
            ((MyPushBackInputStream) input).unread(in);
            logger.info("We pushed pack that element!\n");
            while ((in = (byte) input.read()) != -1) {
                logger.info(in + "\n");
            }
        } catch (IOException e) {
            logger.info("Problems with reading byteArray!");
        }
    }
}
