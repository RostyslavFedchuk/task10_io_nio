package com.epam.pushbackinputsream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.stream.Stream;

import static java.util.Objects.isNull;
import static java.util.Objects.nonNull;

public class MyPushBackInputStream extends InputStream {
    private static Logger logger = LogManager.getLogger(MyPushBackInputStream.class);
    private byte[] byteArray;
    private Stream<byte[]> source;

    public MyPushBackInputStream(byte[] buffer) {
        byteArray = buffer;
        if (nonNull(byteArray)) {
            source = Stream.of(byteArray);
        } else {
            logger.info("Cannot deal with nulls!");
        }
    }

    @Override
    public int read() throws IOException {
        if (isNull(source) || isNull(byteArray)) {
            logger.info("Cannot deal with nulls!");
            return -1;
        }
        if (byteArray.length > 0) {
            byte firstElem = byteArray[0];
            byte[] newBuff = new byte[byteArray.length - 1];
            source = source.skip(1);
            System.arraycopy(byteArray, 1, newBuff, 0, newBuff.length);
            byteArray = newBuff;
            return firstElem;
        } else {
            source.close();
            return -1;
        }
    }

    public void unread(byte b) throws IOException {
        if (isNull(source) || isNull(byteArray)) {
            logger.info("Cannot deal with nulls!");
            return;
        }
        byte[] newByte = new byte[byteArray.length + 1];
        newByte[0] = b;
        for (int i = 1; i < newByte.length; i++) {
            newByte[i] = byteArray[i - 1];
        }
        byteArray = newByte;
        source = Stream.of(byteArray);
    }
}
